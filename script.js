// Creating Symbols
const Clock = document.querySelector('.Clock')
var rotationPos = 0
for(let i=0; i<12; i++){
    const tmp_sym = document.createElementNS('http://www.w3.org/2000/svg', 'line')
    tmp_sym.setAttribute('transform-origin', '200 200') 
    tmp_sym.setAttribute('transform', `rotate(${rotationPos})`) 
    tmp_sym.setAttribute('x1', '200') 
    tmp_sym.setAttribute('x2', '200') 
    tmp_sym.setAttribute('y1', '25') 
    tmp_sym.setAttribute('y2', '0')
    tmp_sym.setAttribute('style', 'stroke:black;stroke-width:9;')
    Clock.appendChild(tmp_sym)
    rotationPos += 30
}

rotationPos = 0
for(let i=0; i<60; i++){
    const tmp_sym = document.createElementNS('http://www.w3.org/2000/svg', 'line')
    tmp_sym.setAttribute('transform-origin', '200 200') 
    tmp_sym.setAttribute('transform', `rotate(${rotationPos})`) 
    tmp_sym.setAttribute('x1', '200') 
    tmp_sym.setAttribute('x2', '200') 
    tmp_sym.setAttribute('y1', '10') 
    tmp_sym.setAttribute('y2', '0')
    tmp_sym.setAttribute('style', 'stroke:black;stroke-width:2;')
    Clock.appendChild(tmp_sym)
    rotationPos += 30/5
}
// End Creating Symbols


// Clock Logic
const Hours = document.getElementById('Hours')
const Minutes = document.getElementById('Minutes')
const Seconds = document.getElementById('Seconds')

var rotationMin = 0, rotationHour = 0

const inter = setInterval(()=>{
    const d = new Date
    var rotationSec = Math.floor(d.getSeconds() + d.getMinutes()*60 + d.getHours()*3600)

    rotationSec = d.getSeconds()*360/60
    Seconds.setAttribute('transform', `rotate(${rotationSec})`)
    rotationMin = d.getMinutes()*360/60
    Minutes.setAttribute('transform', `rotate(${rotationMin})`)
    rotationHour = d.getHours()*360/60
    Hours.setAttribute('transform', `rotate(${rotationHour})`)
    
    
}, 1000)